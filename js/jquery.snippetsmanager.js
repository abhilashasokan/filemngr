/*==========================================
             Snippet Manager
Options
    - Header Text (string) :
    - Footer Text (string) :
==========================================*/
(function($) {
    'use strict';
    $.fn.snippetsManager = function(options) {
        //true added to deep merge as we are using objects as options
        var defaults = {
            headerCaption: "File Manager",
            requestPath: "http://api.elayout.test/v1/resource/user/tree/", //http://118.102.223.114:8080/v1/resource/user/tree/
            articleCsgPath: "",
            articleExcludePath: "",
            articleSimplePath: "",
            filePath: "",
            imagePath: "",
            fileUploadPath: "http://api.elayout.test/v1/image/",
            saveFilePath: "",
            apiKey: "web-908239842388842348",
            corpId:"1",
            data: "text",
            activeelem: "",
            listPath: "",
            /* Image upload */
            maxUploadSize: "1",
            maxUploadWidth: "",
            maxUploadHeight: "",
            dpiWidth: "",
            dpiHeight: "",
            dpi: "", 
            validImage: "jpg,png",
            treeId: 'FileManagerTree', 
            imageTitle: "",
            userId: "",
            dataType: 'general',
            uploadable: '0',
            colorspace: 'CMYK',
            savefolder: '',
        };
        var opts = $.extend(true, {}, defaults, options);
        init();
        return this.each(function(index, element) {
            $(element).bind("click.snippetsManager", function() {
                getContent(element);
                bindEvents(element);
            });
        });


        function init() {
            //create "SnippetsManager" div container and add to "body" tag
            if ($('#SnippetsManager').length <= 0) {
                var $SnippetsManager = $('<div />').appendTo('body').attr('id', 'SnippetsManager').addClass('modal').hide();
                var $modal_content = $('<div />').appendTo($SnippetsManager).addClass('modal-content');
                //Header Section
                var $modal_header = $('<div />').appendTo($modal_content).addClass('modal-header');
                var $headerCaption = $('<h2 />').appendTo($modal_header).text(opts.headerCaption);
                var $headerClose = $('<span />').appendTo($modal_header).addClass('close_icon');
                //Content Section
                var $modal_body = $('<div />').appendTo($modal_content).addClass('modal-body');
                var $modal_body_left = $('<div />').appendTo($modal_body).addClass('modal-body-left');
                //var $modal_body_right = $('<div />').appendTo($modal_body).addClass('modal-body-right');
                var $modal_body_right_preview = $('<div />').appendTo($modal_body).addClass('modal-body-right-preview');

                //Footer Section
                var $modal_footer = $('<div />').appendTo($modal_content).addClass('modal-footer');
                if (opts.data === 'media') {
                    var $modal_footer_media_controls = $('<div />').appendTo($modal_footer).addClass('modal-footer-media-controls').attr('id', 'modalFooterMediaControls');
                    var $uploadForm = $("<form/>", {
                        "name": "modal-media-upload-form",
                        "id": "modal-media-upload-form",
                        "method": "post",
                        "enctype": "multipart/form-data"
                    }).appendTo($modal_footer_media_controls);
                    var $modal_footer_media_controls_upload = $("<input/>", {
                        "type": "file",
                        "name": "file_upload_input",
                        "id": "file_upload_input"
                    }).appendTo($uploadForm).addClass('modal-footer-media-controls-upload fileupload');
                    var $modal_media_button_upload = $("<input/>", {
                        "type": "button",
                        "id": "modal-media-button-upload",
                        "value": "Upload"
                    }).appendTo($uploadForm);
                }

                //File open save operations
                var $modal_footer_controls = $('<div />').appendTo($modal_footer).addClass('modal-footer-controls');
                //var $modal_footer_controls_label = $('<div />').appendTo($modal_footer_controls).addClass('modal-footer-controls-label').text('File Name :');
                //var $modal_footer_controls_text = $('<input />').attr('type', 'text').appendTo($modal_footer_controls).addClass('modal-footer-controls-text').attr('id', 'modal_footer_save_text');
                //var $modal_footer_controls_save = $('<button />').appendTo($modal_footer_controls).addClass('modal-footer-button').text("Save").attr('id', "modal_footer_save");
                var $modal_footer_controls_open = $('<button />').appendTo($modal_footer_controls).addClass('btn btn-primary').text("Load Content").attr('id', "modal_footer_open");
                // var $modal_footer_controls_cancel = $('<button />').appendTo($modal_footer_controls).addClass('btn btn-primary').text("Cancel").attr('id', "modal_footer_cancel");
            }
            //Reset "modal-media-button-upload" previous click events
           
            $(".modal-content .close").click(function() {
                $('#SnippetsManager').hide();
                $('#disablingDiv').hide();
            });

            $("#modal_footer_save").click(function(e) {
                saveContentToFile();
                //$('#SnippetsManager').hide();
                //$('body').removeClass('overlay');
            });
            
            $("#modal_footer_open").unbind("click");           
            $("#modal_footer_open").click(function(e) {
                var result = loadContentToControl();
                if (result === true) {
                    setCurrentFilePath();
                    $('#SnippetsManager').hide();
                    $('#disablingDiv').hide();
                }
                return false;
            });

            $("#modal_footer_cancel").click(function() {
                $('#SnippetsManager').hide();
                $('#disablingDiv').hide();
            });

            $(".close_icon").click(function() {
                $('#SnippetsManager').hide();
                $('#disablingDiv').hide();
            });
        }

        function bindEvents(element) {
            $("#modal-media-button-upload").unbind("click");
            if (opts.data === 'media') {
                $("#modal-media-button-upload").click(function() {
                    var formData = new FormData();
                    var currentPath = opts.articleCsgPath;
                    formData.append('image', $('#file_upload_input')[0].files[0]);
                    if ($('#file_upload_input')[0].files.length > 0) {
                        var file = $('#file_upload_input')[0].files[0];
                        //opts.validImage;
                        var name = file.name;
                        var size = file.size;
                        var type = file.type;
                        var currentFilePath = $(".modal-body-left [aria-selected='true']").attr('data-folder');
                        // if (typeof(currentFilePath) === "undefined") {
                        //     alert('Please select a folder to save!');
                        //     return false;
                        // }


                        var curID = $(element).attr('data-for');
                        var css_width = $('#outerbox #'+curID).width();
                        var css_height = $('#outerbox #'+curID).height();

                        var allow_small_images = $('#outerbox #'+curID).attr('csimageallowsmallimages');
                        if (allow_small_images === undefined || allow_small_images === null) {
                            allow_small_images = 0;
                        }
                        var csimagemayresize = $('#outerbox #'+curID).attr('csimagemayresize');
                        if (csimagemayresize === undefined || csimagemayresize === null) {
                            csimagemayresize = 0;
                        }

                        currentFilePath = escape(currentFilePath);
                        formData.append('currentFilePath', currentFilePath);
                        formData.append('max_size', opts.maxUploadSize);
                        formData.append('max_width', opts.maxUploadWidth);
                        formData.append('max_height', opts.maxUploadHeight);
                        // formData.append('dpiWidth', opts.dpiWidth); 
                        // formData.append('dpiHeight', opts.dpiHeight);
                        
                        formData.append('image_types', opts.validImage);
                        formData.append('css_width', css_width);
                        formData.append('css_height', css_height);
                        formData.append('allow_small_images', allow_small_images);
                        formData.append('may_resize', csimagemayresize);
                        formData.append('min_dpi', opts.dpi);
                        formData.append('validImage', opts.validImage);
                        formData.append('data_type', opts.data);
                        formData.append('color_space', opts.colorspace); 
                        formData.append('upload_folder', currentFilePath); 
                        formData.append('corp_id', opts.corpId); 
                        formData.append('save_folder', opts.savefolder);                          
                        $.ajax({  
                            url: opts.fileUploadPath,
                            type: 'POST',
                            data: formData,
                            processData: false, // tell jQuery not to process the data
                            contentType: false, // tell jQuery not to set contentType
                            success: function(data) {
                                var msg = "";
                                getContent(opts.activeelem);
                                //alert(msg);
                                if(data.data.has_error){
                                    $(document).trigger('admin:frontend:view:load:external:alert',[data.data.msg]);    
                                    return false;
                                }
                                else {
                                    $(document).trigger('admin:frontend:view:load:external:alert',["Image uploaded successfully."]);
                                    return true;
                                }
                                //bindEvents(opts.activeelem);
                            },
                            error: function(data) { 
                                 var msg = "";
                                for (var i = 0; i < data.responseJSON.data.validation.image.length; i++) { 
                                    if (msg.length > 0) {
                                        msg = msg + '\n' + data.responseJSON.data.validation.image[i];
                                    } else {
                                        msg = msg + data.responseJSON.data.validation.image[i];
                                    }
                                }
                                //alert(msg);
                                $(document).trigger('admin:frontend:view:load:external:alert',[msg]);
                            } 
                        });
                    }
                });
            }
        }

        function getContent(element) {
            if ($('#SnippetsManager').css('display') == 'none') {
                $('#SnippetsManager').show();
                $(".modal-body-right").html("");
                $(".modal-body-right-preview").html("");
                $('body').addClass('overlay');
            }
            if (opts.data === 'media') {
                if(opts.uploadable>0){
                    $("#modalFooterMediaControls").show();
                } else {
                    $("#modalFooterMediaControls").hide();
                }       
                $(".modal-footer-controls-label").hide();
                $("#modal_footer_save_text").hide();
            } else {
                $("#modalFooterMediaControls").hide();
                $(".modal-footer-controls-label").show();
                $("#modal_footer_save_text").show();
            }
            if (opts.type === 'save') {
                $("#modal_footer_save").show();
                $("#modal_footer_open").hide();
            } else {
                $("#modal_footer_open").show();
                $("#modal_footer_save").hide();
            }
            loadAjaxContent(element);
        }

        function loadAjaxContent(element) {
            $("#disablingDiv").show();
            opts.activeelem = element;
            $('#SnippetsManager').attr('data-for', $(element).attr('data-for'));
            $('#SnippetsManager').hide();
            var curID = $(element).attr('data-for');
            var isNegative = $('#outerbox #'+curID).attr('articleisnegative');
            var requestPathConfigured;
            if(opts.dataType==='article'){
                var docsreplacefile = $('#viewport #div_template_content #document').attr('docsreplacefile');
                if(typeof(docsreplacefile)==='undefined' || typeof(docsreplacefile)===''){
                    docsreplacefile = '';
                }
                requestPathConfigured =  opts.requestPath + "/resource/replace/tree/" + $('#outerbox #'+curID).attr('articlecsgpath')+"?corp_id="+opts.corpId+"&simple_path="+$('#outerbox #'+curID).attr('articlesimplepath')+"&exclude_path="+$('#outerbox #'+curID).attr('articleexcludepath')+"&replace_path="+docsreplacefile+"&div_id="+opts.treeId+"&data_type="+opts.media+"&data_colorspace="+opts.colorspace;
            }
            else{
                requestPathConfigured = opts.requestPath + opts.articleCsgPath + '?exclude=' + opts.articleExcludePath + '&include=' + opts.articleSimplePath+'&list_path='+opts.listPath+'&_api_key='+opts.apiKey+'&div_id='+opts.treeId+'&corp_id='+opts.corpId+"&data_type="+opts.media+"&data_colorspace="+opts.colorspace; //+'&_userID='+opts.userId
            }
            $.ajax({
                url: requestPathConfigured,
                success: function(data) { 
                    var html = $(data['html']);
                    if (jQuery.trim(html).length > 0)
                    {
                        $(".modal-content .modal-body .modal-body-left").html(html);
                        $('#'+opts.treeId).jstree({
                            'core' : {
                                'loaded_state': true, 
                                'check_callback' : true,                                
                                'themes' : {
                                    'responsive': false
                                }
                            },                           
                            'types' : {
                                'default' : {
                                    'icon' : 'icon-folder'
                                },
                                'file' : {
                                    'icon' : 'icon-link'
                                }
                            },
                        'plugins': ['types', 'dnd','state']
                        })
                        .bind("loaded.jstree", function (event, data) {
                            // you get two params - event & data - check the core docs for a detailed description
                            $(this).jstree("open_all");
                        })
                        .on("open_node.jstree", function (e, data) { 
                            setTimeout(function(){
                                filterColorSpace(element);
                            }, 100);
                         });;
                    }
                    containerCenter();
                    if(isNegative){
                        $(".modal-body-right-preview").removeClass('mini-article-is-negative').addClass('mini-article-is-negative');
                    }
                    $('#SnippetsManager').show();
                    //To load sub folders click
                    $('#SnippetsManager .tree-folder').bind('click',function(e){
                        setTimeout(function(){
                            loadNodeClick();
                        }, 100);
                    });
                    //To load tree node click
                    setTimeout(function(){
                        loadNodeClick();
                        filterColorSpace(element);
                    }, 100);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    console.log("Status: " + textStatus); 
                    console.log("Error: " + errorThrown); 
                    $("#disablingDiv").hide();
                },
                type: 'GET',
            });
        }

        function filterColorSpace(element) {
            if(opts.data === 'media'){
                var curID = $(element).attr('data-for');
                var colorspace = $('#outerbox #'+curID).attr('csimagecolorspace');
                $("#open_popup_tree .tree-file").each(function(i, obj) {
                    var $data_prop = $.parseJSON($(obj).attr("data-prop"));
                    if(typeof($data_prop.color_space)!=='undefined' && $data_prop.color_space!==''){
                        if($data_prop.color_space!==colorspace){
                            $(obj).removeClass("tree-file-disabled").addClass("tree-file-disabled");
                            $(obj).unbind('click');
                        }
                    }
                });
            }
        }
        
        function loadNodeClick() {
            $('#SnippetsManager .tree-file').bind('click',function(e){
                var dataLink = $(e.currentTarget).attr('data-link');
                loadFilePreview(dataLink);
            });
        }

        /**
         * This function loads the file content on to a preview screen
         */

        function loadFilePreview(datalink) {
            if (opts.type === 'open') {
                if (opts.data === 'text') {
                    if (typeof(datalink) !== 'undefined') {
                        $.ajax({
                            url: datalink,
                            success: function(data) {
                                $(".modal-body-right-preview").html($.trim(data['html']));
                            }
                        });
                    }
                }
                else if(opts.data === 'media'){
                    var $preview = $(".modal-body-right-preview");
                    $(".modal-body-right-preview #preview_image").remove();
                    var $imagePreview = $('<img />').appendTo($preview).attr('src', datalink + '?_api_key=' + opts.apiKey + '&action=view').attr('id', 'preview_image');
                }
            }
        }

        /**
         * This function loads the selected file content to control that initiated the call
         */
        function loadContentToControl() {
            if (opts.type === 'open') {
                if (opts.data === 'text') {
                    if ($(".modal-body-right-preview").text()!=='') {
                        $("#outerbox #" + $('#SnippetsManager').attr('data-for')).html($(".modal-body-right-preview").html());
                        $("#outerbox #" + $('#SnippetsManager').attr('data-for')).keyup();
                        $("#disablingDiv").hide();
                        return true;
                    } else {
                        //Convert this to notify method
                        //alert('Please select a file to open');
                        var msg = 'Please select a file to open';
                        $(document).trigger('admin:frontend:view:load:external:alert',[msg]);
                        return false;
                    }
                } else {
                    var $control_id = $("#outerbox #" + $('#SnippetsManager').attr('data-for'));
                    var data_prop  = $(".modal-body-left [aria-selected='true']").attr('data-prop');
                    var validateResult = canValidateImage(data_prop, $control_id); 
                    validateResult = $.parseJSON(validateResult);
                    if(validateResult.hasError){
                        $(document).trigger('admin:frontend:view:load:external:alert',[validateResult.msg]);
                        return false;
                    }
                    else {                        
                        if($("#preview_image").length>0){
                            $("#outerbox #" + $('#SnippetsManager').attr('data-for')).html("");
                            var data_id = "data_"+$control_id.attr('id');
                            $('<img />').appendTo($control_id).attr('id',"data_"+$control_id.attr('id')).attr('class',"userImages").attr('src', $(".modal-body-right-preview img").attr('src')).attr('title', opts.imageTitle).attr("data-prop",data_prop)
                            .on('load', function() {
                                var containerHeight = $control_id.height(); 
                                var containerWidth = $control_id.width();
                                if($("#outerbox #"+data_id).height() > containerHeight && $("#outerbox #"+data_id).height()>0){
                                    $("#outerbox #"+data_id).css({"width":"", "height":containerHeight});
                                }                           
                                if($("#outerbox #"+data_id).width() > containerWidth && $("#outerbox #"+data_id).width() > 0){
                                    $("#outerbox #"+data_id).css({"width":containerWidth, "height":""});  
                                }
                            })
                            .on('error', function() { console.log("error loading image"); });
                            return true;
                        }
                    }                    
                }
            }
        }

        function canValidateImage(data_prop, $control_id){
            var data_prop   = $.parseJSON(data_prop);
            //Control attributes values that we read from control
            var MinDpi      = $control_id.attr('csimagemindpi');
            var maxWidth    = $control_id.attr("csimagemaxwidth");
            var maxHeight   = $control_id.attr("csimagemaxheight");
            var cssWidth    = $control_id.width();
            var cssHeight   = $control_id.height();

            var mayResize           = $control_id.attr("csimagemayresize");
            if (typeof mayResize === 'undefined') {
                mayResize = 0;
            }
            var allowSmallerImages  = $control_id.attr("csimageallowsmallimages"); 
            if (typeof allowSmallerImages === 'undefined') {
                allowSmallerImages = 0;
            }

            //The images attributes that we are receving from server of previously upload images
            var Upload_Image_Width = data_prop.width;
            var Upload_Image_Height = data_prop.height;
            var ImgVarientDpi = data_prop.dpi;
            // Variables needed for process
            var msg;
            var hasError = false;
            var downsize = false;
            var New_ImgVarientDpi;
        
            if (maxWidth > 0 && maxHeight > 0) {
                New_ImgVarientDpi = (Upload_Image_Width / cssWidth) * ImgVarientDpi;
                New_ImgVarientDpi = parseInt(New_ImgVarientDpi);
                if (ImgVarientDpi >= MinDpi) {
                    if(New_ImgVarientDpi<MinDpi && Upload_Image_Width<cssWidth) {
                        if(allowSmallerImages){
                            msg = "Insert the image into CS:Image container with size of " + Upload_Image_Width + "mm X " + Upload_Image_Height + "mm as attributes with "+ImgVarientDpi+"  DPI";
                            var validateResult = setImageStatus(Upload_Image_Height,Upload_Image_Width,ImgVarientDpi,false,msg);
                        }
                        else{
                            msg         = "Error: image to small to upload ("+New_ImgVarientDpi+") as DPI";;
                            var validateResult = setImageStatus("","","",true,msg);
                        }
                    }
                    else if(Upload_Image_Width<=cssWidth){
                        if(allowSmallerImages){
                            var validateResult = setImageStatus(Upload_Image_Height,Upload_Image_Width,ImgVarientDpi,false,msg);
                        }
                        else {
                            msg = "Insert the image into CS:Image container with size of " + cssWidth + "mm X " + cssHeight + "mm as attributes with "+New_ImgVarientDpi+" as DPI";
                            var validateResult = setImageStatus(cssHeight,cssWidth,New_ImgVarientDpi,false,msg);                
                        }
                    }
                    else{
                        msg = "Insert the image into CS:Image container with size of " + Upload_Image_Width + "mm X " + Upload_Image_Height + "mm as attributes with "+ImgVarientDpi+" as DPI";
                        var validateResult = setImageStatus(Upload_Image_Height,Upload_Image_Width,ImgVarientDpi,false,msg);   
                    }                   
                }
                else if (Upload_Image_Width > cssWidth && Upload_Image_Height > cssHeight && New_ImgVarientDpi>=MinDpi) {
                    msg = "Insert the image into CS:Image container with size of " + cssWidth + "mm X " + cssHeight + "mm as attributes with " + New_ImgVarientDpi + " as DPI";  
                    var validateResult = setImageStatus(cssHeight,cssWidth,New_ImgVarientDpi,false,msg);

                }
                else { 
                    if (Upload_Image_Width < cssWidth && Upload_Image_Height < cssHeight && New_ImgVarientDpi < MinDpi) {
                        if(allowSmallerImages){
                            msg         = "Error: resolution to low for upload ("+New_ImgVarientDpi+") as DPI";
                            var validateResult = setImageStatus("","","",true,msg);
                        }
                        else {
                            msg         = "Error: image to small to upload ("+New_ImgVarientDpi+") as DPI";
                            var validateResult = setImageStatus("","","",true,msg);
                        }
                    } else {
                        msg         = "Error: resolution to low for upload ("+New_ImgVarientDpi+") as DPI";
                        var validateResult = setImageStatus("","","",true,msg);
                    }
                }
            } else {
                if (allowSmallerImages) {
                    New_ImgVarientDpi = ImgVarientDpi;
                } else {
                    New_ImgVarientDpi = (Upload_Image_Width / cssWidth) * ImgVarientDpi;
                }
                New_ImgVarientDpi = parseInt(New_ImgVarientDpi);
                if (New_ImgVarientDpi >= MinDpi) {
                    if (allowSmallerImages) {
                        if (Upload_Image_Width < cssWidth && Upload_Image_Height < cssHeight) {
                            msg = "Insert the image into CS:Image container with size of " + Upload_Image_Width + "mm X " + Upload_Image_Height + "mm as attributes with " + New_ImgVarientDpi + " as DPI";
                            var validateResult = setImageStatus(Upload_Image_Height,Upload_Image_Width,New_ImgVarientDpi,false,msg);
                        } else {
                            msg = "Insert the image into CS:Image container with size of " + cssWidth + "mm X " + cssHeight + "mm as attributes with " + New_ImgVarientDpi + " as DPI";
                            var validateResult = setImageStatus(cssHeight,cssWidth,New_ImgVarientDpi,false,msg);
                        }
                    } else {
                        msg = "Insert the image into CS:Image container with size of " + cssWidth + "mm X " + cssHeight + "mm as attributes with " + New_ImgVarientDpi + " as DPI";
                        var validateResult = setImageStatus(cssHeight,cssWidth,New_ImgVarientDpi,false,msg);
                    }
                } else {
                    if (Upload_Image_Width < cssWidth && Upload_Image_Height < cssHeight && New_ImgVarientDpi < MinDpi) {
                        if (allowSmallerImages) {
                            msg         = "Error: resolution to low for upload (" + New_ImgVarientDpi + ") as DPI";
                        } else {
                            msg         = "Error: image to small to upload (" + New_ImgVarientDpi + ") as DPI";
                        }
                        var validateResult = setImageStatus("","","",true,msg);
                    } else {
                        msg             = "Error: resolution to low for upload (" + New_ImgVarientDpi + ") as DPI";
                        var validateResult = setImageStatus("","","",true,msg);
                    }
                }
            }
            return validateResult;
        }

        function setImageStatus(height,width,dpi,hasError,msg) {
            var CssSettings = {
                "height": "",
                "width": "",
                "dpi": "",
                "hasError":"",
                "msg": ""
              }
            CssSettings.height      = height;
            CssSettings.width       = width;
            CssSettings.dpi         = dpi;
            CssSettings.hasError    = hasError;
            CssSettings.msg         = msg;
            return JSON.stringify(CssSettings);
        }

        /**
         * This function save the content of selected text in to selected folder and name
         */
        function saveContentToFile() {
            if (opts.type === 'save') {
                var currentFilePath = $(".modal-body-left [aria-selected='true']").attr('data-folder');;
                if (typeof(currentFilePath) === "undefined") {
                    $(document).trigger('admin:frontend:view:load:external:alert',['Please select a folder to save!']);
                    return false;
                }
                var fileSelected = $("#modal_footer_save_text").val().trim();
                currentFilePath = escape(currentFilePath);
                var currentContent = $("#outerbox #" + $('#SnippetsManager').attr('data-for')).html();
                /*
                var SendInfo = [{
                    currentFilePath: currentFilePath,
                    currentContent: currentContent,
                    currentFileName: fileSelected
                }];
                */
                var regex = new RegExp("^[a-zA-Z_]+$");
                if (!regex.test(fileSelected)) {
                    $(document).trigger('admin:frontend:view:load:external:alert',['Please use a filename without extension!']);
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: opts.saveFilePath,
                    dataType: 'json',
                    data: JSON.stringify({
                        "currentFilePath": currentFilePath,
                        "currentContent": currentContent,
                        "currentFileName": fileSelected
                    }),
                    contentType: "application/json; charset=utf-8",
                    success: function(data) {
                        $('body').removeClass('overlay');
                        $("#modal_footer_save_text").val("");
                        $('#SnippetsManager').hide();
                    }
                });
            }
        }
        /**
         * Load the content to page
         */
        function setCurrentFilePath() {
            var fileSelected = $(".modal-body-right-ul .selected").find('.modal-body-right-li-span').attr('data-url');
            //data-prop
            var data_for = $("#SnippetsManager").attr('data-for');
            $("#" + data_for).attr('data-url', fileSelected);
        }
        /**
         * This function calculates and adjusts the screen position to center
         */

        function containerCenter() {
            $('#SnippetsManager').css({"position":"absolute", "z-index":'11'});
            $('#SnippetsManager').css("top", Math.max(0, (($(window).height() - $('#SnippetsManager').outerHeight()) / 2) + $(window).scrollTop()) + "px");
            $('#SnippetsManager').css("left", Math.max(0, (($(window).width() - $('#SnippetsManager').outerWidth()) / 2) + $(window).scrollLeft()) + "px");
        }
    };
}(jQuery));